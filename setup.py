import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="colors-nightday", # Replace with your own username
    version="0.0.1",
    author="nightday",
    author_email="nightday@internet.ru",
    description="colors!",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/NightDay/colors",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)